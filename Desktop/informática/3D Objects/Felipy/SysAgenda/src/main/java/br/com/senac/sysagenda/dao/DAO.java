package br.com.senac.sysagenda.dao;

import java.util.List;
import javax.persistence.EntityManager;


public abstract class DAO<T> {
    protected EntityManager en ;
    
    private final Class<T> entidade ;
    
    public DAO(Class<T> entidade){
        this.entidade = entidade;
    }
    
    public void save (T entity){
        this.en = JPAUTIL.getEntityManager() ; // abrir conexão
        en.getTransaction().begin(); // começa uma transação
        en.persist(entity); // inserir
        en.getTransaction().commit(); // finalizar uma transação
        en.close(); // fechar conexão
        
    }
    
    public void update (T entity){
           this.en = JPAUTIL.getEntityManager() ; // abrir conexão
        en.getTransaction().begin(); // começa uma transação
        en.merge(entity); // inserir
        en.getTransaction().commit(); // finalizar uma transação
        en.close(); // fechar conexão
        
    }
    
    public void delete (T entity){
            this.en = JPAUTIL.getEntityManager() ; // abrir conexão
        en.getTransaction().begin(); // começa uma transação
        en.remove(en.merge(entity)); // inserir
        en.getTransaction().commit(); // finalizar uma transação
        en.close(); // fechar conexão
        
    }
    
    public T find (long id){
        this.en = JPAUTIL.getEntityManager();
        en.getTransaction().begin();
        T t = en.find(entidade, id); // find
        en.getTransaction().commit();
        en.close();
        return t;
    }
    
    
    public List<T> findAll(){
        this.en = JPAUTIL.getEntityManager();
        List<T> lista;
        en.getTransaction().begin();
        lista = en.createQuery(" from " + entidade.getName() + " e ").getResultList();
        en.getTransaction().commit();
        en.close();
        
        
        return lista;
    }
}



